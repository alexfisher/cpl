<?php
/**
 * @file
 * Returns the HTML for a single Drupal page.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728148
 */
?>


<div id="page"><div id="page-inner">
<div class="region-page-top">
<div id="header"><div id="header-inner" class="clear-block">
<h1 id="logo-title"><a href="<?php print $base_path; ?>" title="Home" rel="home">Canton Public Library</a></h1>
<div id="utilities"><a class="loc-link" href="/location-hours">Location/Hours</a> <a href="/contact">Contact Us</a></div>
</div></div> <!-- /#header-inner, /#header -->

<div id="action-bar">
<div id="block-block-7">
<h2><a href="http://catalog.cantonpl.org/">Catalog</a></h2>
<form action="//catalog.cantonpl.org/search/X" id="cplCat"><fieldset><legend>Search our Catalog</legend>
  <input type="submit" value="" id="cplCatSubmit"/>
  <label for="searcharg" class="invisible">Term</label><input id="searcharg" maxlength="75" name="SEARCH" placeholder="Search Books, DVDs, more" size="24" type="text" value="" />
  <div id="suggestions"></div>
</fieldset></form>
</div><!-- /#block-block-7 -->
</div><!-- /#action-bar -->
</div><!-- /.region-page-top -->

  <div id="main">

    <div id="content" class="column" role="main">
      <?php print render($page['highlighted']); ?>
      <?php print $breadcrumb; ?>
      <a id="main-content"></a>
      <?php print render($title_prefix); ?>
      <?php if ($title): ?>
        <h1 class="page__title title" id="page-title"><?php print $title; ?></h1>
      <?php endif; ?>
      <?php print render($title_suffix); ?>
      <?php print $messages; ?>
      <div class="tabs"><?php print render($tabs); ?></div>
      <?php print render($page['help']); ?>
      <?php if ($action_links): ?>
        <ul class="action-links"><?php print render($action_links); ?></ul>
      <?php endif; ?>
      <?php print render($page['content']); ?>
      <?php print $feed_icons; ?>
    </div>

    <div id="navigation"></div>

    <?php
      // Render the sidebars to see if there's anything in them.
      $sidebar_first  = render($page['sidebar_first']);
      $sidebar_second = render($page['sidebar_second']);
    ?>

    <?php if ($sidebar_first || $sidebar_second): ?>
      <aside class="sidebars">
        <?php print $sidebar_first; ?>
        <?php print $sidebar_second; ?>
      </aside>
    <?php endif; ?>

  </div>

<div id="footer"><div id="footer-inner" class="region region-footer">
<div id="social-media">
  <a class="social-icon facebook" href="http://www.facebook.com/CantonLibrary" title="CPL on Facebook"></a>
  <a class="social-icon flickr" href="http://flickr.com/photos/cantonpubliclibrary" title="CPL on Flickr"></a>
  <a class="social-icon twitter" href="http://twitter.com/CantonLibrary" title="CPL on Twitter"></a>
  <a class="social-icon youtube" href="http://www.youtube.com/user/CantonLibrary" title="CPL on YouTube"></a>
  <a class="social-icon pinterest" href="http://pinterest.com/cantonlibrary/" title="CPL on Pinterest"></a>
  <a class="social-icon email-icon" href="http://eepurl.com/gETIz" title="CPL emails"></a>
  <a class="social-icon rss-icon" href="/blog-browse/feed" title="CPL via RSS"></a>
</div>
<div class="footer-section" id="site-index">
  <h2>Site</h2>
  <form action="/search/node/"  accept-charset="UTF-8" method="post" id="search-form" class="search-form"><fieldset class="form-item" id="edit-search-block-form-1-wrapper"><legend><a href="/search">Search this site</a></legend>
  <label for="edit-keys" class="invisible">Term</label>
  <input type="text" maxlength="128" name="keys" id="edit-keys" size="12" value="" />
  <input type="hidden" name="form_token" id="edit-search-form-form-token" value="<?php print drupal_get_token('search_form'); ?>"  />
  <input type="hidden" name="form_id" id="edit-search-form" value="search_form"  />
  <input type="submit" name="op" id="cpl-search-submit" value="" />
  </fieldset></form>
</div><!-- /#site-index -->
<div class="footer-section" id="hours">
  <h2>Hours</h2>
  <span class="days">Mon-Thurs</span><span class="hourange">9:00 AM-9:00 PM</span><br />
  <span class="days">Fri &amp; Sat</span><span class="hourange">9:00 AM-6:00 PM</span><br />
  <span class="days">Sun</span><span class="hourange">Noon-6:00 PM</span>
</div><!-- /#hours -->
<div class="footer-section" id="location">
  <h2>Location</h2>
  <div class="vcard">
  <a href="/aboutus/hours.html" title="Additional information"><span id="orgname" class="fn org">Canton Public Library</span><br/>
  <span class="adr"><span class="street-address">1200 S. Canton Center Rd.</span><br/>
  <span class="locality">Canton</span>, <abbr class="region" title="Michigan">MI</abbr> <span class="postal-code">48188</span>
  </span></a><!-- /adr --><br/>
  <strong class="tel"><span class="invisible type">work</span>734-397-0999</strong><!-- /tel -->
  <a href="mailto:askcpl@cantonpl.org" class="email invisible">askcpl@cantonpl.org</a>
  </div><!-- /vcard -->
  <span class="invisible geo"><span class="invisible latitude">42.297294091852564</span>,<span class="invisible longitude">-83.48774671554565</span></span>
</div><!-- /#location -->
</div></div><!-- /#footer-inner, /#footer -->

</div>

<?php print render($page['bottom']); ?>
