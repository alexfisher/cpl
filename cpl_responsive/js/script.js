/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.my_custom_behavior = {
  attach: function(context, settings) {
    // Resize events
    var resize_timer;

    function resize_function() {
      if($(window).width() <= 480){
        var sidebarLeft = $(".region-sidebar-first");
        if(sidebarLeft.siblings("#toggle-nav").length == 0){
          sidebarLeft.before("<div id=\"toggle-nav\">Show Menu</div>");
          sidebarLeft.hide();

          $("#toggle-nav").on("click",function(e){ // switch to delegation on live > jQ1.3
            var toggleNav = $(this);
            toggleNav.siblings(".region-sidebar-first").slideToggle("slow",function(){
              (toggleNav.text() == 'Hide Menu') ? toggleNav.text('Show Menu') : toggleNav.text('Hide Menu');
            });
          });
        }
      }
      else{
        $(".region-sidebar-first").show();
        $("#toggle-nav").remove();
      }
    }

    // React when screen is resized
    $(window).resize(function() {
      resize_function();
    });

    // Decide what to do during screen load
    resize_function();

  }
};


})(jQuery, Drupal, this, this.document);

