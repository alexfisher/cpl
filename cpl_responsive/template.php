<?php
/**
 * @file
 * Contains the theme's functions to manipulate Drupal's default markup.
 *
 * Complete documentation for this file is available online.
 * @see https://drupal.org/node/1728096
 */


/**
 * Override or insert variables into the maintenance page template.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("maintenance_page" in this case.)
 */
/* -- Delete this line if you want to use this function
function cpl_responsive_preprocess_maintenance_page(&$variables, $hook) {
  // When a variable is manipulated or added in preprocess_html or
  // preprocess_page, that same work is probably needed for the maintenance page
  // as well, so we can just re-use those functions to do that work here.
  cpl_responsive_preprocess_html($variables, $hook);
  cpl_responsive_preprocess_page($variables, $hook);
}
// */

/**
 * Override or insert variables into the html templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("html" in this case.)
 */
/* -- Delete this line if you want to use this function
function cpl_responsive_preprocess_html(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');

  // The body tag's classes are controlled by the $classes_array variable. To
  // remove a class from $classes_array, use array_diff().
  //$variables['classes_array'] = array_diff($variables['classes_array'], array('class-to-remove'));
}
// */

/**
 * Override or insert variables into the page templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("page" in this case.)
 */
/* -- Delete this line if you want to use this function
function cpl_responsive_preprocess_page(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */


/**
 * Override or insert variables into the node templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("node" in this case.)
 */

function cpl_responsive_preprocess_node(&$variables, $hook) {
  // $variables['sample_variable'] = t('Lorem ipsum.');

  if ($variables['submitted']) {
    $variables['date'] = format_date($variables['node']->created,'date_only');
 //   $variables['name'] = l(htmlspecialchars_decode($variables['name']), 'blog/' . $variables['node']->uid);
 //   $variables['name'] = theme('username', array(link_path => 'blog/' . $variables['node']->uid));
    $variables['submitted'] = t('!datetime | !username', array(
      '!datetime' => $variables['date'], 
      '!username' => $variables['name']
      ));
  }


  // Optionally, run node-type-specific preprocess functions, like
  // cpl_responsive_preprocess_node_page() or cpl_responsive_preprocess_node_story().
  // $function = __FUNCTION__ . '_' . $variables['node']->type;
  //if (function_exists($function)) {
  // $function($variables, $hook);
  // }
}


/**
 * Override or insert variables into the comment templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("comment" in this case.)
 */
/* -- Delete this line if you want to use this function
function cpl_responsive_preprocess_comment(&$variables, $hook) {
  $variables['sample_variable'] = t('Lorem ipsum.');
}
// */

/**
 * Override or insert variables into the region templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("region" in this case.)
 */
/* -- Delete this line if you want to use this function
function cpl_responsive_preprocess_region(&$variables, $hook) {
  // Don't use Zen's region--sidebar.tpl.php template for sidebars.
  //if (strpos($variables['region'], 'sidebar_') === 0) {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('region__sidebar'));
  //}
}
// */

/**
 * Override or insert variables into the block templates.
 *
 * @param $variables
 *   An array of variables to pass to the theme template.
 * @param $hook
 *   The name of the template being rendered ("block" in this case.)
 */
/* -- Delete this line if you want to use this function
function cpl_responsive_preprocess_block(&$variables, $hook) {
  // Add a count to all the blocks in the region.
  // $variables['classes_array'][] = 'count-' . $variables['block_id'];

  // By default, Zen will use the block--no-wrapper.tpl.php for the main
  // content. This optional bit of code undoes that:
  //if ($variables['block_html_id'] == 'block-system-main') {
  //  $variables['theme_hook_suggestions'] = array_diff($variables['theme_hook_suggestions'], array('block__no_wrapper'));
  //}
}
// */

/**
 * Implements theme_field__field_type().
 */
function cpl_responsive_field__taxonomy_term_reference($variables) {
  $output = '';


  if (strpos($variables['classes'],'field-name-field-audience')) {
    $output .= '<ul class="taxonomy clear-both"><li class="taxonomy"><i class="fa fa-fw fa-user"></i></li> ';

    foreach ($variables['items'] as $delta => $item) {
      $output .= '<li class="taxonomy term-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>   ';
    }
    $output .= '</ul>';
    $output .= '<ul class="taxonomy"><li class="taxonomy"><i class="fa fa-fw fa-tags"></i></li> ';
  }

/*
else {
    $output .= '<ul class="taxonomy"><li class="taxonomy"><i class="fa fa-fw fa-tags"></i></li> ';
}

  // Render the items.
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy term-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>   ';
  }
  $output .= '</ul>';*/

  // Render the top-level DIV.
//  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] .'>' . $output . '</div>';

  else {
      foreach ($variables['items'] as $delta => $item) {
        $output .= '<li class="taxonomy term-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>   ';
      }

  }

  if (strpos($variables['classes'],'field-name-field-tags')) {
    $output .= '</ul>';
  }

  return $output;
}


/**
 * Render a "Submitted by"-line.
 */
function cpl_responsive_render_submitted_by($field) {
  $account = user_load($field['entity']->uid);
  switch ($field['formatter']) {
    case 'ds_time_ago':
      $interval = REQUEST_TIME - $field['entity']->created;
      return t('Submitted !interval ago by !user.', array('!interval' => format_interval($interval), '!user' => theme('username', array('account' => $account))));
    default:
      $date_format = str_replace('ds_post_date_', '', $field['formatter']);
      return t('<i class="fa fa-fw fa-angle-right"></i> !date | !user', array('!date' => format_date($field['entity']->created, $date_format), '!user' => theme('username', array('account' => $account))));
  }
}