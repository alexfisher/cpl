<?php
/**
 * @file
 * cpl_programs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cpl_programs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cpl_programs_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function cpl_programs_node_info() {
  $items = array(
    'program' => array(
      'name' => t('Program'),
      'base' => 'node_content',
      'description' => t('Create blog posts for programs'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'program_sessions' => array(
      'name' => t('Program Sessions'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
