/* JavaScript for admin views */

(function ($) {
  Drupal.behaviors.cpl_admin = {
    attach: function (context, settings) {
      var start_date = $('input#edit-field-program-sessions-und-form-field-program-sessions-date-und-0-value-datepicker-popup-0');
      if (start_date.length > 0) {
        $(start_date).change(function() {
          var end_date = $('input#edit-field-program-sessions-und-form-field-program-sessions-date-und-0-value2-datepicker-popup-0');
          $(end_date).val(this.value);
        });
      }
    }
  }

})(jQuery);