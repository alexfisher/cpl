<?php
/**
 * @file
 * cys.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function cys_default_rules_configuration() {
  $items = array();
  $items['rules_save_mega_concentration_badge'] = entity_import('rules_config', '{ "rules_save_mega_concentration_badge" : {
      "LABEL" : "Save Mega Concentration Badge",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cys" ],
      "REQUIRES" : [ "cys", "rules" ],
      "ON" : { "node_insert--cys_log" : { "bundle" : "cys_log" } },
      "IF" : [
        { "user_has_all_concentration" : {
            "user" : [ "node:author" ],
            "concentration" : [ "node:field-cys-user-badge" ]
          }
        }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "cys_log",
              "param_title" : "[node:author] has earned the Mega ",
              "param_author" : [ "node:author" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-cys-user-badge" ],
            "value" : [ "node:field-cys-user-badge" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : "1" } },
        { "entity_fetch" : {
            "USING" : { "type" : "badge", "id" : [ "entity-created:field-cys-user-badge:id" ] },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "drupal_message" : { "message" : "\\u003Cimg src=\\u0022[entity-fetched:field_cys_badge_image]\\u0022 style=\\u0022float:right;\\u0022 \\/\\u003E For earning the Super Bookworm, On the Scene, and E-Lectrified versions of this badge, you have unlocked the [entity-fetched:title] badge! As a reward, enjoy this \\u003Ca href=\\u0022http:\\/\\/youtu.be\\/[entity-fetched:field_video_id]\\u0022\\u003Especial surprise video\\u003C\\/a\\u003E! Then head on back to \\u003Ca href=\\u0022\\/connect-your-summer\\u0022\\u003Ethe Connect Your Summer page\\u003C\\/a\\u003E to keep up the good work.\\r\\n\\u003Ciframe width=\\u0022600\\u0022 height=\\u0022400\\u0022 src=\\u0022https:\\/\\/www.youtube.com\\/embed\\/[entity-fetched:field_video_id]\\u0022 frameborder=\\u00220\\u0022 allowfullscreen\\u003E\\u003C\\/iframe\\u003E" } }
      ]
    }
  }');
  $items['rules_save_mega_path_badge'] = entity_import('rules_config', '{ "rules_save_mega_path_badge" : {
      "LABEL" : "Save Mega Path Badge",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "cys" ],
      "REQUIRES" : [ "cys", "rules" ],
      "ON" : { "node_insert--cys_log" : { "bundle" : "cys_log" } },
      "IF" : [
        { "user_has_all_path" : { "user" : [ "node:author" ], "path" : [ "node:field-cys-user-badge" ] } }
      ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "cys_log",
              "param_title" : "[node:author] has earned the Super ",
              "param_author" : [ "node:author" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-cys-user-badge" ],
            "value" : [ "node:field-cys-user-badge" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : "1" } },
        { "entity_fetch" : {
            "USING" : { "type" : "badge", "id" : [ "entity-created:field-cys-user-badge:id" ] },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "drupal_message" : { "message" : "\\u003Cimg src=\\u0022[entity-fetched:field_cys_badge_image]\\u0022 style=\\u0022float:right;\\u0022 \\/\\u003E For earning all of the badges along one path, you have unlocked the [entity-fetched:title] badge! As a reward, enjoy this \\u003Ca href=\\u0022http:\\/\\/youtu.be\\/[entity-fetched:field_video_id]\\u0022\\u003Especial surprise video\\u003C\\/a\\u003E! Then head on back to \\u003Ca href=\\u0022\\/connect-your-summer\\u0022\\u003Ethe Connect Your Summer page\\u003C\\/a\\u003E to keep up the good work.\\r\\n\\u003Ciframe width=\\u0022600\\u0022 height=\\u0022400\\u0022 src=\\u0022https:\\/\\/www.youtube.com\\/embed\\/[entity-fetched:field_video_id]\\u0022 frameborder=\\u00220\\u0022 allowfullscreen\\u003E\\u003C\\/iframe\\u003E" } }
      ]
    }
  }');
  $items['rules_save_ultra_badge'] = entity_import('rules_config', '{ "rules_save_ultra_badge" : {
      "LABEL" : "Save Ultra Badge",
      "PLUGIN" : "reaction rule",
      "WEIGHT" : "10",
      "OWNER" : "rules",
      "TAGS" : [ "cys" ],
      "REQUIRES" : [ "cys", "rules" ],
      "ON" : { "node_insert--cys_log" : { "bundle" : "cys_log" } },
      "IF" : [ { "user_has_all_but_ultra" : { "user" : [ "node:author" ] } } ],
      "DO" : [
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "cys_log",
              "param_title" : "[node:author] received the Super Mega Ultra Summer badge!",
              "param_author" : [ "node:author" ]
            },
            "PROVIDE" : { "entity_created" : { "entity_created" : "Created entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "entity-created:field-cys-user-badge" ],
            "value" : [ "node:field-cys-user-badge" ]
          }
        },
        { "entity_save" : { "data" : [ "entity-created" ], "immediate" : "1" } },
        { "entity_fetch" : {
            "USING" : { "type" : "badge", "id" : [ "entity-created:field-cys-user-badge:id" ] },
            "PROVIDE" : { "entity_fetched" : { "entity_fetched" : "Fetched entity" } }
          }
        },
        { "drupal_message" : { "message" : "\\u003Cimg src=\\u0022[entity-fetched:field_cys_badge_image]\\u0022 style=\\u0022float:right;\\u0022 \\/\\u003E For earning the Super Bookworm, On the Scene, and E-Lectrified badges, you have unlocked the [entity-fetched:title] badge! As a reward, enjoy this \\u003Ca href=\\u0022http:\\/\\/youtu.be\\/[entity-fetched:field_video_id]\\u0022\\u003Especial surprise video\\u003C\\/a\\u003E! \\r\\n\\u003Ciframe width=\\u0022600\\u0022 height=\\u0022400\\u0022 src=\\u0022https:\\/\\/www.youtube.com\\/embed\\/[entity-fetched:field_video_id]\\u0022 frameborder=\\u00220\\u0022 allowfullscreen\\u003E\\u003C\\/iframe\\u003E" } }
      ]
    }
  }');
  $items['rules_show_message_after_first_log_entry'] = entity_import('rules_config', '{ "rules_show_message_after_first_log_entry" : {
      "LABEL" : "Show message after first log entry",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "cys", "rules" ],
      "ON" : { "node_insert--cys_log" : { "bundle" : "cys_log" } },
      "IF" : [ { "user_has_only_one_badge" : { "user" : [ "node:author" ] } } ],
      "DO" : [
        { "drupal_message" : {
            "message" : "Congratulations on your first CYS badge! Just a quick reminder that all winners will be notified by email, so \\u003Ca href=\\u0022user\\/[node:author:uid]\\/edit\\u0022\\u003Echeck your settings\\u003C\\/a\\u003E and make sure your email address is up to date. ",
            "repeat" : "0"
          }
        }
      ]
    }
  }');
  return $items;
}
