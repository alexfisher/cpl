<?php
/**
 * @file
 * cys.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function cys_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function cys_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_eck_bundle_info().
 */
function cys_eck_bundle_info() {
  $items = array(
    'badge_cys2015' => array(
      'machine_name' => 'badge_cys2015',
      'entity_type' => 'badge',
      'name' => 'cys2015',
      'label' => 'cys2016',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'created' => 0,
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function cys_eck_entity_type_info() {
  $items = array(
    'badge' => array(
      'name' => 'badge',
      'label' => 'Badge',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
      ),
    ),
  );
  return $items;
}

/**
 * Implements hook_node_info().
 */
function cys_node_info() {
  $items = array(
    'cys_log' => array(
      'name' => t('CYS Log'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('What did you do?'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
