<?php

/**
 * Implements hook_rules_category_info().
 */
function cys_rules_category_info() {
  return array(
    'cys' => array(
      'label' => t('CYS'),
      'equals group' => t('CYS'),
    ),
  );
}

/**
 * Implementation of hook_rules_condition_info()
 */
function cys_rules_condition_info() {
  return array(
    'user_has_all_path' => array(
      'label' => t('User has all badges in path'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('Specifies the user for which to evaluate the condition, e.g. the author.'),
          'restriction' => 'selector',
        ),
        'path' => array(
          'type' => 'entity',
          'label' => t('Badge to check'),
          'description' => t('The badge to be check against user badges, specified by using a data selector.'),
          'allow null' => FALSE,
          'restriction' => 'selector',
        ),
      ),
      'group' => t('CYS'),
      'base' => 'cys_condition_user_has_all_path',
    ),
    'user_has_all_concentration' => array(
      'label' => t('User has all badges in concentration'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('Specifies the user for which to evaluate the condition, e.g. the author.'),
          'restriction' => 'selector',
        ),
        'concentration' => array(
          'type' => 'entity',
          'label' => t('Badge to check'),
          'description' => t('The badge to be check against user badges, specified by using a data selector.'),
          'allow null' => FALSE,
          'restriction' => 'selector',
        ),
      ),
      'group' => t('CYS'),
      'base' => 'cys_condition_user_has_all_concentration',
    ),
    'user_has_all_but_ultra' => array(
      'label' => t('User has all badges but ultra'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('Specifies the user for which to evaluate the condition, e.g. the author.'),
          'restriction' => 'selector',
        ),
      ),
      'group' => t('CYS'),
      'base' => 'cys_condition_user_has_all_but_ultra',
    ),
    'user_has_only_one_badge' => array(
      'label' => t('User has only one badge'),
      'parameter' => array(
        'user' => array(
          'type' => 'user',
          'label' => t('User'),
          'description' => t('Specifies the user for which to evaluate the condition, e.g. the author.'),
          'restriction' => 'selector',
        ),
      ),
      'group' => t('CYS'),
      'base' => 'cys_condition_user_has_only_one_badge',
    ),
  );
}

/**
 * Implements hook_rules_action_info_alter().
 */
function cys_rules_action_info_alter(&$actions) {
  if (isset($actions['drupal_message'])) {
    $actions['drupal_message']['base'] = 'cys_rules_action_drupal_message';
  }
}

/**
 * Condition cys_condition_user_has_all_path
 */
function cys_condition_user_has_all_path($user, $path) {
  $path = entity_load_single('badge', $path->id->value());
  if(empty($path->field_cys_path[LANGUAGE_NONE])) {
    return FALSE;
  }
  return cys_condition_user_has_all_but_super_mega_ultra($user,
         $path->field_cys_path[LANGUAGE_NONE][0]['tid'], 'path');
}

/**
 * Condition cys_condition_user_has_all_concentration
 */
function cys_condition_user_has_all_concentration($user, $concentration) {
  $concentration = entity_load_single('badge', $concentration->id->value());
  if(empty($concentration->field_cys_concentration)) {
    return FALSE;
  }
  return cys_condition_user_has_all_but_super_mega_ultra($user,
         $concentration->field_cys_concentration[LANGUAGE_NONE][0]['tid'], 'concentration');
}

/**
 * Condition cys_condition_user_has_all_but_ultra
 */
function cys_condition_user_has_all_but_ultra($user) {
  $ultra = get_first_term_id('super mega ultra', 'cys_super_mega_ultra_tags');
  if(cys_user_has_super_mega_ultra($user, $ultra)) {
    return FALSE;
  }
  $mega = get_first_term_id('mega', 'cys_super_mega_ultra_tags');
  $super = get_first_term_id('super', 'cys_super_mega_ultra_tags');
  if (cys_user_has_super_mega_ultra($user, $mega) &&
      cys_user_has_super_mega_ultra($user, $super)) {
    return TRUE;
  }
  return FALSE;
}

/**
 * Check either the row or column for all badges gained except the mega or ultra
 */
function cys_condition_user_has_all_but_super_mega_ultra($user, $term_id, $direction) {
  $super_mega = get_first_term_id($direction == 'path' ? 'super' : 'mega', 'cys_super_mega_ultra_tags');
  $ultra = get_first_term_id('super mega ultra', 'cys_super_mega_ultra_tags');
  if (cys_user_has_super_mega($user, $term_id, 'cys_' . $direction, $super_mega) ||
      cys_user_has_super_mega($user, $term_id, 'cys_' . $direction, $ultra)) {
    return FALSE;
  }
  return cys_user_has_all_badges_except_smu($user, $term_id, 'cys_' . $direction);
}

/**
 * Check either the row or column for all badges gained except the mega or ultra
 */
function cys_condition_user_has_only_one_badge($user) {
  $logs = entity_load('node', array(), array('uid' => $user->uid, 'type' => 'cys_log'));
  if (count($logs) > 1) {
    return FALSE;
  }
  return TRUE;
}

/**
 * Action: Show a drupal message.
 */
function cys_rules_action_drupal_message($message, $status, $repeat) {
  drupal_set_message($message, $status, $repeat);
}