<?php
/**
 * @file
 * Code for the CYS views.
 */

/**
 * Implementation of hook_views_pre_render().
 * TODO: find real view name and change
 */
function cys_views_pre_render(&$view) {
  global $user;
  if($view->name == 'badge_view_2015') {
    if (cys_user_has_badge($user, $view->args[0]) && user_access('claim cys badge')) {
      // User has already earned this badge
      $view->field['nothing']->options['alter']['text'] = "Share More";
    }
    elseif (user_access('claim cys badge')) {
      // User has yet to earn this badge
      $filename = str_replace('png', 'gray.png', $view->result[0]->field_field_cys_badge_image[0]['rendered']['#item']['filename']);
      $uri = str_replace('png', 'gray.png', $view->result[0]->field_field_cys_badge_image[0]['rendered']['#item']['uri']);

      $view->result[0]->field_field_cys_badge_image[0]['rendered']['#item']['filename'] = $filename;
      $view->result[0]->field_field_cys_badge_image[0]['rendered']['#item']['uri'] = $uri;
    }
    else { // This hides our "claim" or "share more" button during the off-season
      $view->field['nothing']->options['alter']['alter_text'] = FALSE;
      $view->field['nothing']->options['alter']['text'] = "";
    }
    if (!empty($view->result[0]->field_field_super_mega_ultra_status)) {  // Hide the claim button when the badge is super/mega/ultra
      $view->field['nothing']->options['alter']['alter_text'] = FALSE;
      $view->field['nothing']->options['alter']['text'] = "";
    }
  }

  if ($view->name == 'all_badges') {
    foreach ($view->result as &$result) {
      $result->field_field_cys_badge_image[0]['rendered']['#path']['path'] = 'badge/' . compro_custom_get_path_alias_title(entity_load_single('badge', $result->id));
      if (!cys_user_has_badge($user, $result->id) && isset($result->field_field_cys_badge_image[0])) {

        // User has yet to earn this badge
        $filename = str_replace('png', 'gray.png', $result->field_field_cys_badge_image[0]['rendered']['#item']['filename']);
        $uri = str_replace('png', 'gray.png', $result->field_field_cys_badge_image[0]['rendered']['#item']['uri']);

        $result->field_field_cys_badge_image[0]['rendered']['#item']['filename'] = $filename;
        $result->field_field_cys_badge_image[0]['rendered']['#item']['uri'] = $uri;
      }
    }
  }
}

/**
 * Implementation of hook_views_query_alter().
 */
function cys_views_query_alter(&$view, &$query) {
  $badge_id_variable = '';
  if (isset($view->query->where[0]['conditions'][0]['value']) &&
      is_array($view->query->where[0]['conditions'][0]['value'])) {
    foreach ($view->query->where[0]['conditions'][0]['value'] as $index => $id) {
      $badge_id_variable = $index;
    }
  }

  if ($view->name == 'badge_view_2015' && isset($view->query->where[0])) {
    if (isset($view->query->where[0]['conditions'][0]['value'][$badge_id_variable]) &&
      !is_numeric($view->query->where[0]['conditions'][0]['value'][$badge_id_variable])) {
      $view->query->where[0]['conditions'][0]['value'][$badge_id_variable] =
        get_badge_id_from_url();
    }
  }
  if($view->name == 'badge_booklists_2016' && isset($view->query->where[0])) {
    if (isset($view->query->where[0]['conditions'][0]['value'][$badge_id_variable]) &&
      !is_numeric($view->query->where[0]['conditions'][0]['value'][$badge_id_variable])) {
      $view->query->where[0]['conditions'][0]['value'][$badge_id_variable] =
        get_badge_id_from_url();
    }
  }
  if ($view->name == 'user_badge_logs' && $view->current_display == 'block_3' && isset($view->query->where[0]) ) {
    if (isset($view->query->where[0]['conditions'][0]['value'][$badge_id_variable]) &&
      !is_numeric($view->query->where[0]['conditions'][0]['value'][$badge_id_variable])) {
      $view->query->where[0]['conditions'][0]['value'][$badge_id_variable] =
        get_badge_id_from_url();
    }
  }
  if ($view->name == 'user_badge_logs' && $view->current_display == 'page_1' && isset($view->query->where[0])) {
    if (isset($view->query->where[0]['conditions'][0]['value'][$badge_id_variable]) &&
      !is_numeric($view->query->where[0]['conditions'][0]['value'][$badge_id_variable])) {
      $view->query->where[0]['conditions'][0]['value'][$badge_id_variable] =
        get_badge_id_from_url();
    }
  }
}

/**
 * Dissect the views URL and turn that phrase into a badge ID
 */
function get_badge_id_from_url() {
  $tokens = explode('/', str_replace(':', '', $_GET['q']));
  $pattern = pathauto_pattern_load_by_entity('badge', 'cys2015');
  $prefix = explode('/', $pattern);
  $badge = drupal_get_normal_path($prefix[0] . '/' . $tokens[array_search('badge',$tokens) + 1]);
  $norm_badge = explode('/', $badge);
  return sizeof($norm_badge) == 3 ? $norm_badge[2] : $norm_badge[1];
}