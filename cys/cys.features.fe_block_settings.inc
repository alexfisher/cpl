<?php
/**
 * @file
 * cys.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function cys_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['views-badge_booklists_2016-block_1'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_1',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-super-bookworm
badge/be-creative-super-bookworm
badge/chow-down-super-bookworm
badge/game-super-bookworm
badge/geek-out-super-bookworm
badge/great-outdoors-super-bookworm
badge/hit-road-super-bookworm
badge/keep-it-real-super-bookworm
badge/laugh-it-super-bookworm
badge/explore-history-super-bookworm
badge/my-mitten-super-bookworm
badge/you-choose-super-bookworm',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -18,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_10'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_10',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-e-lectrified
badge/be-creative-e-lectrified
badge/chow-down-e-lectrified
badge/game-e-lectrified
badge/geek-out-e-lectrified
badge/great-outdoors-e-lectrified
badge/hit-road-e-lectrified
badge/keep-it-real-e-lectrified
badge/laugh-it-e-lectrified
badge/explore-history-e-lectrified
badge/my-mitten-e-lectrified
badge/you-choose-e-lectrified',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -13,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_11'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_11',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-e-lectrified
badge/be-creative-e-lectrified
badge/chow-down-e-lectrified
badge/game-e-lectrified
badge/geek-out-e-lectrified
badge/great-outdoors-e-lectrified
badge/hit-road-e-lectrified
badge/keep-it-real-e-lectrified
badge/laugh-it-e-lectrified
badge/explore-history-e-lectrified
badge/my-mitten-e-lectrified
badge/you-choose-e-lectrified',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -12,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_12'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_12',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-e-lectrified
badge/be-creative-e-lectrified
badge/chow-down-e-lectrified
badge/game-e-lectrified
badge/geek-out-e-lectrified
badge/great-outdoors-e-lectrified
badge/hit-road-e-lectrified
badge/keep-it-real-e-lectrified
badge/laugh-it-e-lectrified
badge/explore-history-e-lectrified
badge/my-mitten-e-lectrified
badge/you-choose-e-lectrified',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -11,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_2'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_2',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-super-bookworm
badge/be-creative-super-bookworm
badge/chow-down-super-bookworm
badge/game-super-bookworm
badge/geek-out-super-bookworm
badge/great-outdoors-super-bookworm
badge/hit-road-super-bookworm
badge/keep-it-real-super-bookworm
badge/laugh-it-super-bookworm
badge/explore-history-super-bookworm
badge/my-mitten-super-bookworm
badge/you-choose-super-bookworm',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -17,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_3'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_3',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-scene
badge/be-creative-scene
badge/chow-down-scene
badge/game-scene
badge/geek-out-scene
badge/great-outdoors-scene
badge/hit-road-scene
badge/keep-it-real-scene
badge/laugh-it-scene
badge/explore-history-scene
badge/my-mitten-scene
badge/you-choose-scene',
    'roles' => array(
      'staff' => 4,
    ),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -23,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_4'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_4',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-scene
badge/be-creative-scene
badge/chow-down-scene
badge/game-scene
badge/geek-out-scene
badge/great-outdoors-scene
badge/hit-road-scene
badge/keep-it-real-scene
badge/laugh-it-scene
badge/explore-history-scene
badge/my-mitten-scene
badge/you-choose-scene',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -22,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_5'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_5',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-scene
badge/be-creative-scene
badge/chow-down-scene
badge/game-scene
badge/geek-out-scene
badge/great-outdoors-scene
badge/hit-road-scene
badge/keep-it-real-scene
badge/laugh-it-scene
badge/explore-history-scene
badge/my-mitten-scene
badge/you-choose-scene',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -21,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_6'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_6',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-scene
badge/be-creative-scene
badge/chow-down-scene
badge/game-scene
badge/geek-out-scene
badge/great-outdoors-scene
badge/hit-road-scene
badge/keep-it-real-scene
badge/laugh-it-scene
badge/explore-history-scene
badge/my-mitten-scene
badge/you-choose-scene',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -20,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_7'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_7',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-e-lectrified
badge/be-creative-e-lectrified
badge/chow-down-e-lectrified
badge/game-e-lectrified
badge/geek-out-e-lectrified
badge/great-outdoors-e-lectrified
badge/hit-road-e-lectrified
badge/keep-it-real-e-lectrified
badge/laugh-it-e-lectrified
badge/explore-history-e-lectrified
badge/my-mitten-e-lectrified
badge/you-choose-e-lectrified',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -15,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_8'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_8',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-e-lectrified
badge/be-creative-e-lectrified
badge/chow-down-e-lectrified
badge/game-e-lectrified
badge/geek-out-e-lectrified
badge/great-outdoors-e-lectrified
badge/hit-road-e-lectrified
badge/keep-it-real-e-lectrified
badge/laugh-it-e-lectrified
badge/explore-history-e-lectrified
badge/my-mitten-e-lectrified
badge/you-choose-e-lectrified',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -16,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  $export['views-badge_booklists_2016-block_9'] = array(
    'cache' => -1,
    'css_class' => '',
    'custom' => 0,
    'delta' => 'badge_booklists_2016-block_9',
    'module' => 'views',
    'node_types' => array(),
    'pages' => 'badge/animal-lover-e-lectrified
badge/be-creative-e-lectrified
badge/chow-down-e-lectrified
badge/game-e-lectrified
badge/geek-out-e-lectrified
badge/great-outdoors-e-lectrified
badge/hit-road-e-lectrified
badge/keep-it-real-e-lectrified
badge/laugh-it-e-lectrified
badge/explore-history-e-lectrified
badge/my-mitten-e-lectrified
badge/you-choose-e-lectrified',
    'roles' => array(),
    'themes' => array(
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'cpl_responsive' => array(
        'region' => 'content',
        'status' => 1,
        'theme' => 'cpl_responsive',
        'weight' => -14,
      ),
      'seven' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'seven',
        'weight' => 0,
      ),
      'zen' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'zen',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 1,
  );

  return $export;
}
