<?php
/**
 * @file
 * cys.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function cys_taxonomy_default_vocabularies() {
  return array(
    'cys_concentration' => array(
      'name' => 'CYS Concentration',
      'machine_name' => 'cys_concentration',
      'description' => '',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'cys_path_tags' => array(
      'name' => 'CYS Path Tags',
      'machine_name' => 'cys_path_tags',
      'description' => 'Tags that reference different paths available in CYS. ',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'cys_super_mega_ultra_tags' => array(
      'name' => 'CYS Super Mega Ultra Tags',
      'machine_name' => 'cys_super_mega_ultra_tags',
      'description' => 'A list of tags for classifying if a badge is of the super, mega, or ultra variety. ',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
