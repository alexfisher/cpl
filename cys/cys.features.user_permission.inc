<?php
/**
 * @file
 * cys.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function cys_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'claim cys badge'.
  $permissions['claim cys badge'] = array(
    'name' => 'claim cys badge',
    'roles' => array(
      'administrator' => 'administrator',
      'staff' => 'staff',
    ),
    'module' => 'cys',
  );

  return $permissions;
}
